DROP DATABASE IF EXISTS exchange_item;
CREATE DATABASE exchange_item;
USE exchange_item;
SET SQL_SAFE_UPDATES=1;


CREATE TABLE `user` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(64) NOT NULL,
	`last_name` VARCHAR(64) NOT NULL,
	`email` VARCHAR(64) NOT NULL UNIQUE,
	`passwd` VARCHAR(64) NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `item` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`main_img_id` INT,
	`category_id` INT NOT NULL,
	`name_id` INT NOT NULL,
	`description_id` INT NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `image` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`src` VARCHAR(64) NOT NULL,
--	`item_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `src` (`src`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `offer` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`item_id` INT NOT NULL,
	`state` VARCHAR(32) NOT NULL,
	`time` DATE NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `response` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`item_id` INT NOT NULL,
	`state` VARCHAR(32) NOT NULL,
	`offer_id` INT NOT NULL,
	`time` DATE NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `category` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(64) NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `description` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`content` VARCHAR(256) NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `name` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`content` VARCHAR(32) NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `bag` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`item_id` INT NOT NULL,
	PRIMARY KEY (`id`))
	ENGINE=INNODB DEFAULT CHARSET=utf8;

ALTER TABLE `item` ADD CONSTRAINT `item_fk0` FOREIGN KEY (`main_img_id`) REFERENCES `image`(`id`) ON DELETE CASCADE;

ALTER TABLE `item` ADD CONSTRAINT `item_fk1` FOREIGN KEY (`category_id`) REFERENCES `category`(`id`) ON DELETE CASCADE;

ALTER TABLE `item` ADD CONSTRAINT `item_fk2` FOREIGN KEY (`name_id`) REFERENCES `name`(`id`) ON DELETE CASCADE;

ALTER TABLE `item` ADD CONSTRAINT `item_fk3` FOREIGN KEY (`description_id`) REFERENCES `description`(`id`) ON DELETE CASCADE;

--ALTER TABLE `image` ADD CONSTRAINT `image_fk0` FOREIGN KEY (`item_id`) REFERENCES `item`(`id`) ON DELETE CASCADE;

ALTER TABLE `offer` ADD CONSTRAINT `offer_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE;


ALTER TABLE `offer` ADD CONSTRAINT `offer_fk1` FOREIGN KEY (`item_id`) REFERENCES `item`(`id`);

ALTER TABLE `response` ADD CONSTRAINT `offer_response_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `response` ADD CONSTRAINT `offer_response_fk1` FOREIGN KEY (`item_id`) REFERENCES `item`(`id`);

ALTER TABLE `response` ADD CONSTRAINT `offer_response_fk2` FOREIGN KEY (`offer_id`) REFERENCES `offer`(`id`);

ALTER TABLE `bag` ADD CONSTRAINT `bag_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE;

ALTER TABLE `bag` ADD CONSTRAINT `bag_fk1` FOREIGN KEY (`item_id`) REFERENCES `item`(`id`) ON DELETE CASCADE;

commit;

create user 'working' identified by '12345';
grant all on exchange_item.* to 'working';
commit;

