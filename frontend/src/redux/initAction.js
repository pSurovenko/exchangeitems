import { getMe, getCategory, getAllItemId, getAllOffers, getMyResponse } from '../components/service/api';

export const setMyData = MeData => {
  return {
    type: 'Logged',
    userId: MeData.id,
    logged: true,
    firstName: MeData.firstName,
    lastName: MeData.lastName,
    myItemsId: MeData.myItemsId,
  };
};

export const setCategories = categories => {
  return {
    type: 'categoryUp',
    category: categories,
  };
};

export const setAllItemId = AllItemsId => {
  return {
    type: 'setAllItems',
    allItemsId: AllItemsId,
  };
};

export const SetAllOffer = (allOffersId, myOffersId, myItemInOfferId, myActiveOfferIds, myOfferMyItemRelations, allActiveOffer) => {
  return {
    type: 'SetAllOffer',
    allOffersId: allOffersId,
    myOffersId: myOffersId,
    myItemInOffer: myItemInOfferId,
    myActiveOfferIds: myActiveOfferIds,
    myOfferMyItemRelations: myOfferMyItemRelations,
    allActiveOffer: allActiveOffer,
  };
};

export const SetMyResponse = (itemInResponseId, activeResponseId, allMyResponseId) => {
  return {
    type: 'initResponses',
    itemInResponseId: itemInResponseId,
    activeResponseId: activeResponseId,
    allMyResponseId: allMyResponseId,
  };
};

export function fetchMe() {
  return dispatch =>
    getMe().then(MeData => {
      dispatch(setMyData(MeData));
    });
}

export function categoryUp() {
  return dispatch =>
    getCategory().then(categories => {
      dispatch(setCategories(categories));
    });
}

export function fetchAllItemsId() {
  return dispatch =>
    getAllItemId().then(AllItemsId => {
      dispatch(setAllItemId(AllItemsId));
    });
}

export function fetchAllOfferId() {
  return dispatch =>
    getAllOffers().then(AllOffers => {
      dispatch(SetAllOffer(AllOffers.all, AllOffers.my, AllOffers.myItemsInOfferId, AllOffers.myActiveOfferIds, AllOffers.myOfferMyItemRelations, AllOffers.allActiveOffer));
    });
}

export function fetchMyResponseId() {
  return dispatch =>
    getMyResponse().then(MyResponse => {
      dispatch(SetMyResponse(MyResponse.itemInResponseId, MyResponse.activeResponseId, MyResponse.allMyResponseId));
    });
}
