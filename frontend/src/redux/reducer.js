const initialState = {
  userId: null,
  userFirstName: null,
  userLastName: null,
  logged: false,
  category: [],
  myItemsId: [],
  allItemsId: [],
  myOffersId: [],
  allOffersId: [],
  myItemInOffer: [],
  myActiveOfferIds: [],
  myOfferMyItemRelations: [],
  allActiveOffer: [],

  myActiveResponse: [],
  myResponse: [],
  myItemInResponse: [],
};

export default function Reducer(state = initialState, action) {
  switch (action.type) {
    case 'Logged':
      return { ...state, userId: action.userId, logged: action.logged, userFirstName: action.firstName, userLastName: action.lastName, myItemsId: action.myItemsId };
    case 'UnLogged':
      return { ...state, userId: action.userId, logged: action.logged, userFirstName: action.firstName, userLastName: action.lastName, myItemsId: action.myItemsId };
    case 'categoryUp':
      return { ...state, category: action.category };
    case 'itemAdded':
      return {
        ...state,
        myItemsId: [...state.myItemsId, action.itemId],
      };
    case 'setAllItems':
      return {
        ...state,
        allItemsId: action.allItemsId,
      };
    case 'createOffer':
      return {
        ...state,
        allActiveOffer: [...state.allActiveOffer, action.offerId],
        myOffersId: [...state.myOffersId, action.offerId],
        allOffersId: [...state.allOffersId, action.offerId],
        myActiveOfferIds: [...state.myActiveOfferIds, action.offerId],
        myItemInOffer: [...state.myItemInOffer, action.itemId],
        myOfferMyItemRelations: [...state.myOfferMyItemRelations, { offerId: action.offerId, itemId: action.itemId }],
      };
    case 'SetAllOffer':
      return {
        ...state,
        allOffersId: action.allOffersId,
        myOffersId: action.myOffersId,
        myItemInOffer: action.myItemInOffer,
        myActiveOfferIds: action.myActiveOfferIds,
        myOfferMyItemRelations: [...state.myOfferMyItemRelations, ...action.myOfferMyItemRelations],
        allActiveOffer: action.allActiveOffer,
      };
    case 'cancelMyOffer':
      return {
        ...state,
        allActiveOffer: state.allActiveOffer.filter(id => id !== action.offerId),
        myActiveOfferIds: state.myActiveOfferIds.filter(id => id !== action.offerId),
        myItemInOffer: state.myItemInOffer.filter(id => id !== action.itemId),
        myOfferMyItemRelations: state.myOfferMyItemRelations.filter(relation => relation.itemId !== action.itemId && relation.offerId !== action.offerId),
      };
    case 'createResponse':
      return {
        ...state,
        myActiveResponse: [...state.myActiveResponse, action.responseId],
        myResponse: [...state.myActiveResponse, action.responseId],
        myItemInResponse: [...state.myItemInResponse, action.itemId],
      };
    case 'initResponses':
      return {
        ...state,
        myActiveResponse: action.activeResponseId,
        myResponse: action.allMyResponseId,
        myItemInResponse: action.itemInResponseId,
      };
    case 'cancelResponse':
      return {
        ...state,
        myActiveResponse: state.myActiveResponse.filter(id => id !== action.responseId),
        myItemInResponse: state.myItemInResponse.filter(id => id !== action.itemId),
      };

    default:
      return state;
  }
}
