export const login = (id, firstName, lastName, myItemsId) => ({
  type: 'Logged',
  userId: id,
  logged: true,
  firstName: firstName,
  lastName: lastName,
  myItemsId: myItemsId,
});

export const unLogin = () => ({
  type: 'UnLogged',
  id: null,
  logged: false,
  firstName: null,
  lastName: null,
  myItemsId: [],
});

export const addItemAction = itemId => ({
  type: 'itemAdded',
  itemId: itemId,
});

export const createOfferAction = (offerId, itemId) => ({
  type: 'createOffer',
  offerId: offerId,
  itemId: itemId,
});

export const cancelMyOffer = (offerId, itemId) => ({
  type: 'cancelMyOffer',
  offerId: offerId,
  itemId: itemId,
});

export const createResponseAction = (responseId, itemId) => ({
  type: 'createResponse',
  responseId: responseId,
  itemId: itemId,
});

export const cancelResponse = (responseId, itemId) => ({
  type: 'cancelResponse',
  responseId: responseId,
  itemId: itemId,
});
