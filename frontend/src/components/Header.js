const axios = require('axios');
import React, { useState, useEffect } from 'react';
import BackButton from './Nav/BackButton';
import ForwardButton from './Nav/ForwardButton';
const greeting = 'Добро пожаловать!';
import MiniMe from './MiniMe';
export default function Header() {
  return (
    <header className="header">
      <div className="top_nav">
        <div className="logo">
          {' '}
          <a href="/">HOME!</a>{' '}
        </div>
        <BackButton />
        <ForwardButton />
      </div>

      <div className="content">{greeting}</div>
      <div>
        <MiniMe />
      </div>
    </header>
  );
}
