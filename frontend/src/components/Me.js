const axios = require('axios');
import React, { useState, useEffect } from 'react';

function Me() {
  const [id, setId] = useState(null);
  const [email, setEmail] = useState('Unauthenticated@mail.com');
  const [firstName, setFirstName] = useState('Unauthenticated');
  const [lastName, setLastName] = useState('Unauthenticated');
  useEffect(() => {
    axios
      .get('/api/me')
      .then(response => {
        if (response.status == 200) {
          setId(response.data.id);
          setEmail(response.data.email);
          setFirstName(response.data.firstName);
          setLastName(response.data.lastName);
        }
      })
      .catch();
  }, []);

  return (
    <div>
      <p> Me: </p>
      <p> id: {id} </p>
      <p> email: {email} </p>
      <p> firstName: {firstName}</p>
      <p> lastName: {lastName}</p>
    </div>
  );
}

export default Me;
