import React, { useState } from 'react';
import { Redirect } from '@reach/router';
import { connect } from 'react-redux';

import { login, unLogin } from '../redux/actions';
const axios = require('axios');

const mapStateToProps = function(state) {
  return {
    logged: state.logged,
  };
};
const mapDispatchToProps = function(dispatch) {
  return {
    unLogin: () => {
      dispatch(unLogin());
    },
  };
};

function Logout(props) {
  axios.post('/api/logout').then(() => {
    props.unLogin();
  });
  return (
    <div>
      <Redirect to="/reg" />
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Logout);
