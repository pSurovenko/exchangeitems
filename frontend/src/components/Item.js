import React, { Component, useState, useEffect } from 'react';
const axios = require('axios');
import { connect } from 'react-redux';
import { login, unLogin } from '../redux/actions';
import ButtonForAddItem from '../scenes/ItemStore/ButtonForAddItem';
import ButtonForCreateOffer from '../scenes/Offers/ButtonForCreateOffer';
import ButtonForCancelOffer from '../scenes/Offers/ButtonForCancelOffer';
import Image from './Image';

const mapStateToProps = function(state) {
  return {
    myItemsId: state.myItemsId,
    myItemInOffer: state.myItemInOffer,
    myActiveOfferIds: state.myActiveOfferIds,
  };
};

const Item = connect(
  mapStateToProps,
  { login, unLogin },
)(function item(props) {
  const [ItemData, setItemData] = useState(null);
  const [ImageSrc, setImageSrc] = useState(null);

  useEffect(() => {
    axios
      .get('/api/item/' + props.id.toString())
      .then(response => {
        setItemData(response.data);
        axios.get('/api/image/' + String(response.data.image_id)).then(result => {
          setImageSrc(result.data);
        });
      })
      .catch();
  }, []);

  if (ItemData === null) {
    return <div />;
  }

  return (
    <div className="item" onClick={props.onclick}>
      <p>id: {ItemData.id}</p>
      <Image src={ImageSrc} altText={ItemData.name.content} />
      <p> category: {ItemData.category.name}</p>
      <p> name: {ItemData.name.content}</p>
      <p>description: {ItemData.description.content}</p>

      {props.answerable &&
        ((props.myItemsId.indexOf(props.id) === -1 && <ButtonForAddItem id={props.id} />) ||
          (props.myItemInOffer.indexOf(props.id) === -1 && <ButtonForCreateOffer id={props.id} />) ||
          (<p className="itemInOffer"> Предмет в предложениях на обмен...</p> && <ButtonForCancelOffer id={props.id} />))}
    </div>
  );
});

export default Item;
