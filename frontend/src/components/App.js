import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import '../styles/components/App.css';
import { Router, Link } from '@reach/router';

import Header from './Header';
import Items from './Items';
import Nav from './Nav/Nav';
import Me from './Me';
import Entry from '../scenes/Entry/Entry';
import Logout from './Logout';
import MyBag from '../scenes/Bag/MyBag';
import ItemStore from '../scenes/ItemStore/ItemStore';
import OfferStore from '../scenes/Offers/OfferStore';
import { fetchMe, categoryUp, fetchAllItemsId, fetchAllOfferId, fetchMyResponseId } from '../redux/initAction';
import Answers from '../scenes/Offers/Answer/Answers';

function App({ fetchMe, categoryUp, fetchAllItemsId, fetchAllOfferId, fetchMyResponseId }) {
  useEffect(() => {
    fetchMe();
    categoryUp();
    fetchAllItemsId();
    fetchAllOfferId();
    fetchMyResponseId();
  }, []);

  return (
    <div className="app">
      <Header />
      <Nav />
      <div className="content">
        content
        <Router>
          <Items path="item" />
          <ItemStore path="item_store" />
          <OfferStore path="offer_store" />
          <Answers path="answers" />
          <Me path="me" />
          <MyBag path="myBags" />
          <Entry path="reg" />
          <Logout path="logout" />
        </Router>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  logged: state.logged,
  firstName: state.userFirstName,
  lastName: state.userLastName,
  AllCategories: state.category,
  AllItemsId: state.AllItemsId,
});

export default connect(
  mapStateToProps,
  { fetchMe, categoryUp, fetchAllItemsId, fetchAllOfferId, fetchMyResponseId },
)(App);
