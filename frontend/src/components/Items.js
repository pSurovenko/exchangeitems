import React, { Component } from 'react';
import Item from './Item';
import { connect } from 'react-redux';
import { login, unLogin } from '../redux/actions';

const mapStateToProps = function(state) {
  return {
    allItemsId: state.allItemsId,
  };
};

class BlankItems extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.allItemsId.map(id => (
          <div key={id}>
            <Item id={id} answerable={true} />
          </div>
        ))}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  {},
)(BlankItems);
