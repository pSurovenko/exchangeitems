import React from 'react';
import Button from '../Button';
import { navigate } from '@reach/router';

function BackButton(props) {
  return (
    <Button
      name="Back"
      action={() => {
        window.history.back();
      }}
    />
  );
}

export default BackButton;
//used in header

//reach
{
  /*<button onClick={()=>{window.history.back()}}></button>*/
}
