import React, { Component } from 'react';
import { Router, Link } from '@reach/router';

class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
    };
  }

  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="me">Me</Link>
          </li>
          <li>
            <Link to="myBags">My Bag</Link>
          </li>
          <li>
            <Link to="user">User</Link>
          </li>
          <li>
            <Link to="item_store">Item Store</Link>
          </li>
          <li>
            <Link to="offer_store">Offer Store</Link>
          </li>
          <li>
            <Link to="answers">Answers</Link>
          </li>
          <li>
            <Link to="reg">REG</Link>
          </li>
          <li>
            <Link to="logout">Logout</Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default Nav;
