import React from 'react';
import Button from '../Button';

function ForwardButton(props) {
  return (
    <Button
      name="Forward"
      action={() => {
        window.history.forward();
      }}
    />
  );
}

export default ForwardButton;
//used in header
