import axios from 'axios';
const openOfferState = 'open';

export function getMe() {
  return axios.get('/api/user/my_bag').then(bagResponse => {
    let myItemsId = [];
    bagResponse.data.forEach(bagItem => {
      myItemsId.push(bagItem.item_id);
    });
    return axios.get('/api/me').then(result => {
      result.data.myItemsId = myItemsId;
      return result.data;
    });
  });
}

export function getCategory() {
  return axios.get('/api/item/category').then(result => {
    return result.data;
  });
}

export function getAllItemId() {
  return axios.get('/api/item/ids').then(result => {
    return result.data;
  });
}

export function getAllOffers() {
  return axios.get('/api/offer/').then(result => {
    let allOffersId = [];
    let allActiveOffer = [];
    result.data.forEach(objId => {
      allOffersId.push(objId.id);
      if (objId.state === openOfferState) {
        allActiveOffer.push(objId.id);
      }
    });
    return axios.get('/api/offer/my').then(resultMyOffer => {
      let myOfferIds = [];
      let myActiveOfferIds = [];
      let myItemsInOfferId = [];
      let myOfferMyItemRelations = [];
      resultMyOffer.data.forEach(myOffer => {
        if (myOffer.state === openOfferState) {
          myActiveOfferIds.push(myOffer.id);
          myItemsInOfferId.push(myOffer.item_id);
          myOfferMyItemRelations.push({ offerId: myOffer.id, itemId: myOffer.item_id });
        }

        myOfferIds.push(myOffer.id);
      });
      result.data.my = myOfferIds;
      result.data.all = allOffersId;
      result.data.myItemsInOfferId = myItemsInOfferId;
      result.data.myActiveOfferIds = myActiveOfferIds;
      result.data.myOfferMyItemRelations = myOfferMyItemRelations;
      result.data.allActiveOffer = allActiveOffer;

      return result.data;
    });
  });
}

export function getMyResponse() {
  return axios.get('/api/offer/0/offer_response/my').then(result => {
    let itemInResponseId = [];
    let allMyResponseId = [];
    let activeResponseId = [];

    result.data.forEach(response => {
      if (response.state === 'open') {
        itemInResponseId.push(response.item_id);
        activeResponseId.push(response.id);
      }
      allMyResponseId.push(response.id);
    });
    result.data.itemInResponseId = itemInResponseId;
    result.data.activeResponseId = activeResponseId;
    result.data.allMyResponseId = allMyResponseId;

    return result.data;
  });
}
