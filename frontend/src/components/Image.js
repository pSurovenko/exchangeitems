import React from 'react';
const axios = require('axios');

function Image(props) {
  const requestPath = '/img/' + props.src;
  return <img src={requestPath} alt={props.altText} height={250} width={300} />;
}

export default Image;
