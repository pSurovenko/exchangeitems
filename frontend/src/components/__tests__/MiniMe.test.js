import React from 'react'
import { render, fireEvent, cleanup } from 'react-testing-library';

import 'react-testing-library/extend-expect';
import MiniMe  from '../MiniMe';

afterEach(cleanup);

describe('MiniMe', () => {
    it('Render MiniMe', () => {

        const firstName = 'TestFirstName';
        const lastName = 'TestLastName';
        const logged = true;
        const props = {
            firstName : firstName,
            lastName : lastName,
            logged: logged,
        };
        const {getByText} =  render( <MiniMe {...props}> );

        expect(getByText(firstName).textContent).toBe(firstName);
        expect(getByText(lastName).textContent).toBe(lastName);
        expect(getByText(logged).textContent).toBe(logged);

    });

});
