import React from 'react';
import { connect } from 'react-redux';
import { login, unLogin } from '../redux/actions';

const mapStateToProps = function(state) {
  return {
    logged: state.logged,
    firstName: state.userFirstName,
    lastName: state.userLastName,
  };
};

const miniMe = connect(
  mapStateToProps,
  { login, unLogin },
)(function MiniMe(props) {
  return (
    <div className="user_mini">
      <p> {props.firstName} </p>
      <p> {props.lastName} </p>
      <p> logged: {props.logged.toString()}</p>
    </div>
  );
});

export default miniMe;
