import React, { Component, useState, useEffect } from 'react';
import SearchForm from './forms/SearchForm';
import WorkshopForm from './forms/WorkshopForm';

export default function ItemLibrary() {
  return (
    <div>
      <p>Workshop: </p>
      <WorkshopForm />
      <p>ITEM Library</p>
      <SearchForm />
    </div>
  );
}
