import React, { Component } from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import { categoryUp } from '../../../redux/actions';
import Items from '../../../components/Items';
import Item from '../../../components/Item';
import Button from '../../../components/Button';
import { fetchAllItemsId } from '../../../redux/initAction';

function checkAllDone(itemName, category, description, file) {
  return itemName && category && description && file;
}

class BlankWorkshopForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { itemName: '', category: null, description: '', file: '' };

    this.handleChangeForCategory = this.handleChangeForCategory.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeForName = this.handleChangeForName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
  }

  handleImageChange(event) {
    event.preventDefault();

    let files = event.target.files;
    let reader = new FileReader();
    reader.readAsDataURL(files[0]);
    this.setState({ file: event.target.files[0] });
  }

  handleChangeForCategory(event) {
    this.setState({ category: event.target.value });
  }

  handleChangeDescription(event) {
    this.setState({ description: event.target.value });
  }

  handleChangeForName(event) {
    this.setState({ itemName: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    if (!checkAllDone(this.state.itemName, this.state.category, this.state.description, this.state.file)) {
      alert('заполните все поля!');
      return;
    }
    let xhr = new window.XMLHttpRequest();
    let fd = new FormData();
    fd.append('fileUpload', this.state.file);
    fd.append('action', 'uploadImage');
    xhr.open('post', '/api/image/upload', true);
    xhr.send(fd);
    xhr.onreadystatechange = () => {
      if (xhr.readyState == 4) {
        axios
          .post('/api/image/', { src: xhr.responseText })
          .then(response => {
            axios
              .post('/api/item/', {
                itemDescription: this.state.description,
                itemName: this.state.itemName,
                main_img_id: response.data,
                category_id: this.state.category,
              })
              .then(result => {
                this.props.fetchAllItemsId();
                return;
              });
          })
          .catch(err => {
            alert('error');
            console.log(err);
          });
      }
    };
  }

  render() {
    // let {imagePreviewUrl} = this.state;
    // let $imagePreview = null;
    // if (imagePreviewUrl) {
    //     $imagePreview = (<img src={imagePreviewUrl} />);
    // } else {
    //     $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    // }

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {/*{$imagePreview}*/}
          <label>
            Image:
            <input type="file" accept=".jpg , .jepg , .png" onChange={event => this.handleImageChange(event)} />
          </label>

          <label>
            Category:
            <select onChange={this.handleChangeForCategory}>
              {this.props.allCategories.map(category => (
                <option key={category.id} value={category.id}>
                  {category.name}
                </option>
              ))}
            </select>
          </label>
          <label>
            Item Name:
            <input name="ItemName" type="text" value={this.state.itemName} onChange={this.handleChangeForName} />
          </label>
          <label>
            Item Description:
            <input name="ItemDescription" type="text" value={this.state.description} onChange={this.handleChangeDescription} />
          </label>
          <input type="submit" value="Создать" />
        </form>
      </div>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    allCategories: state.category,
  };
};

export default connect(
  mapStateToProps,
  { fetchAllItemsId },
)(BlankWorkshopForm);
