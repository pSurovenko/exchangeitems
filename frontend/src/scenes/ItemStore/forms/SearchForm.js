import React, { Component } from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import Items from '../../../components/Items';
import Item from '../../../components/Item';
import Button from '../../../components/Button';

class BlankSearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: null, category: null, allNames: [], dataForRequest: {}, items: null };

    this.handleChangeForCategory = this.handleChangeForCategory.bind(this);
    this.handleChangeForName = this.handleChangeForName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeCancelSearch = this.handleChangeCancelSearch.bind(this);
  }

  componentDidMount() {
    this.setState({ items: <Items /> });
    axios
      .get('/api/item/name')
      .then(response => {
        this.setState({ allNames: response.data });
      })
      .catch();
  }

  handleChangeForCategory(event) {
    this.setState({ category: event.target.value });
  }

  handleChangeForName(event) {
    this.setState({ name: event.target.value });
  }

  handleChangeCancelSearch() {
    this.setState({ items: <Items /> });
  }

  handleSubmit(event) {
    const dataForRequest = {};
    if (this.state.category !== null) {
      dataForRequest.category_id = this.state.category;
    }
    if (this.state.name !== null) {
      dataForRequest.name_id = this.state.name;
    }

    axios.post('/api/item/find', dataForRequest).then(response => {
      let searchedItems = [];
      response.data.forEach(item => {
        searchedItems.push(<Item id={item.id} />);
      });
      this.setState({ items: searchedItems });
    });
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Category:
            <select onChange={this.handleChangeForCategory}>
              {this.props.allCategories.map(category => (
                <option key={category.id} value={category.id}>
                  {category.name}
                </option>
              ))}
            </select>
          </label>
          <label>
            Name:
            <select onChange={this.handleChangeForName}>
              {this.state.allNames.map(name => (
                <option key={name.id} value={name.id}>
                  {name.content}
                </option>
              ))}
            </select>
          </label>
          <input type="submit" value="Выбрать" />
        </form>
        <Button
          name="очистить"
          action={() => {
            this.handleChangeCancelSearch();
          }}
        />
        {this.state.items}
      </div>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    allCategories: state.category,
  };
};

export default connect(
  mapStateToProps,
  {},
)(BlankSearchForm);
