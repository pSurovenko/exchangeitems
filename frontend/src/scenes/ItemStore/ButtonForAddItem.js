import React from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import { login, unLogin, addItemAction } from '../../redux/actions';
import Button from '../../components/Button';

const mapStateToProps = function(state) {
  return {
    myItemsId: state.myItemsId,
  };
};

const ButtonForAddItem = connect(
  mapStateToProps,
  { login, unLogin, addItemAction },
)(function BlankButtonForAddItem(props) {
  function addItem(id) {
    axios.post('/api/user/my_bag', { item_id: id }).then(response => {
      if (response.status === 200) {
        props.addItemAction(id);
        alert('предмет добавлен');
      }
    });
  }

  return (
    <Button
      name="Добавить"
      action={() => {
        addItem(props.id);
      }}
    />
  );
});

export default ButtonForAddItem;
