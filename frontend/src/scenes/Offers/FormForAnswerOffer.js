import React, { Component, useState, useEffect } from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import { login, unLogin, createResponseAction } from '../../redux/actions';
import Button from '../../components/Button';
import Item from '../../components/Item';

const mapStateToProps = function(state) {
  return {
    myItemsId: state.myItemsId,
    myOfferMyItemRelations: state.myOfferMyItemRelations,
    myItemInOffer: state.myItemInOffer,
    myItemInResponse: state.myItemInResponse,
  };
};

const FormForAnswerOffer = connect(
  mapStateToProps,
  { createResponseAction },
)(function BlankFormForAnswerOffer(props) {
  const [MyFreeItem, setMyFreeItem] = useState(null);
  const [ItemForAnswerId, setItemForAnswerId] = useState(null);

  useEffect(() => {
    let freeItem = [];
    props.myItemsId.forEach(id => {
      if (props.myItemInOffer.indexOf(id) === -1 && props.myItemInResponse.indexOf(id) === -1) {
        freeItem.push(id);
      }
    });
    setMyFreeItem(freeItem);
  }, []);

  function chooseItemForAnswer(idOfItem) {
    setItemForAnswerId(idOfItem);
  }

  function setAnswer() {
    if (!ItemForAnswerId) {
      alert('выберите предмет для обмена!');
      return;
    }
    if (!props.offerId) {
      alert('выберите предложение от другого пользователя!');
      return;
    }

    axios
      .post('/api/offer/' + props.offerId + '/offer_response', {
        item_id: ItemForAnswerId,
      })
      .then(response => {
        if (response.status === 200) {
          props.createResponseAction(response.data, ItemForAnswerId);
          alert('Ответ отправлен!');
        }
      })
      .catch();
  }

  if (MyFreeItem === null) {
    return <div />;
  }

  return (
    <div>
      <p>Items: </p>
      {MyFreeItem.map(id => (
        <Item
          onclick={() => {
            chooseItemForAnswer(id);
          }}
          key={id}
          id={id}
          answerable={false}
        />
      ))}
      <div>
        <p> выбрано предложение № {props.offerId} </p>
        <p>для ответа вы выбрали предмет № {ItemForAnswerId} </p>
        <Button
          name="Ответить на предложение!"
          action={() => {
            setAnswer();
          }}
        />
      </div>
    </div>
  );
});

export default FormForAnswerOffer;
