import React, { Component, useState, useEffect } from 'react';
const axios = require('axios');
import { connect } from 'react-redux';
import Response from './Response';

const mapStateToProps = function(state) {
  return {
    myOfferMyItemRelations: state.myOfferMyItemRelations,
    myItemsId: state.myItemsId,
    myItemInOffer: state.myItemInOffer,
    myActiveOfferIds: state.myActiveOfferIds,
    userId: state.userId,
  };
};

const OutResponse = connect(
  mapStateToProps,
  {},
)(function outResponse(props) {
  const [ResponsesData, setResponsesData] = useState(null);

  useEffect(() => {
    axios
      .get('/api/offer/0/offer_response/my')
      .then(responses => {
        setResponsesData(responses.data);
      })
      .catch();
  }, []);

  if (ResponsesData === null) {
    return <div />;
  }

  return (
    <div className="OutResponse">
      <p>Исходящие</p>
      {ResponsesData.map(id => (
        <div key={id}>
          <Response offer_id={id.offer_id} state={id.state} time={id.time} item_id={id.item_id} user_id={id.user_id} id={id.id} />
        </div>
      ))}
    </div>
  );
});

export default OutResponse;
