import React, { Component, useState, useEffect } from 'react';
import OutResponse from './OutResponse';
import IncomingResponse from './IncomingResponse';

export default function Answers() {
  return (
    <div className="answersBlock">
      <OutResponse />
      <IncomingResponse />
    </div>
  );
}
