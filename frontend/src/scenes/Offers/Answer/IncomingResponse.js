import React, { Component, useState, useEffect } from 'react';
const axios = require('axios');
import { connect } from 'react-redux';
import Response from './Response';

const mapStateToProps = function(state) {
  return {
    myOfferMyItemRelations: state.myOfferMyItemRelations,
    myItemsId: state.myItemsId,
    myItemInOffer: state.myItemInOffer,
    myActiveOfferIds: state.myActiveOfferIds,
    userId: state.userId,
  };
};

const IncomingResponse = connect(
  mapStateToProps,
  {},
)(function incomingResponse(props) {
  const [ResponsesData, setResponsesData] = useState([]);
  var temp = [];
  useEffect(() => {
    props.myActiveOfferIds.forEach(idOfOffer => {
      axios
        .post('/api/offer/0/offer_response/find', { offer_id: idOfOffer })
        .then(response => {
          temp = [...temp, ...response.data];
          setResponsesData([...temp]);
        })
        .catch();
    });
  }, []);

  if (ResponsesData === []) {
    return <div />;
  }

  return (
    <div className="IncomingResponse">
      <p>Входящие</p>
      {ResponsesData.map(id => (
        <div key={id}>
          <Response offer_id={id.offer_id} state={id.state} time={id.time} item_id={id.item_id} user_id={id.user_id} id={id.id} />
        </div>
      ))}
    </div>
  );
});

export default IncomingResponse;
