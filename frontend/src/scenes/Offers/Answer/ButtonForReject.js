import React from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import Button from '../../../components/Button';
import { fetchAllItemsId, fetchAllOfferId, fetchMyResponseId } from '../../../redux/initAction';

const mapStateToProps = function(state) {
  return {
    myActiveResponse: state.myActiveResponse,
    myItemInResponse: state.myItemInResponse,
    myResponse: state.myResponse,
  };
};

const ButtonForRejectResponse = connect(
  mapStateToProps,
  { fetchAllOfferId, fetchMyResponseId, fetchAllItemsId },
)(function BlankForAcceptResponse(props) {
  function acceptResponse(id, offerId) {
    axios.post(`/api/offer/${offerId}/offer_response/${id}`, { action: 'reject' }).then(response => {
      //[0]
      if (response.status === 200) {
        if (response.data) {
          fetchAllItemsId();
          fetchAllOfferId();
          fetchMyResponseId();
          alert(`Обмен отклонен!`);
        }
      }
    });
  }

  return (
    <Button
      name="Отклонить ответ!"
      action={() => {
        acceptResponse(props.id, props.offerId);
      }}
    />
  );
});

export default ButtonForRejectResponse;
