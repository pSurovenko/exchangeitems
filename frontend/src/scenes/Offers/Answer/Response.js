import React, { Component, useState, useEffect } from 'react';
const axios = require('axios');
import { connect } from 'react-redux';
import Item from '../../../components/Item';
import Offer from '../Offer';
import ButtonForCancelResponse from './ButtonForCencel';
import ButtonForAcceptResponse from './ButtonForAccept';
import ButtonForRejectResponse from './ButtonForReject';

const mapStateToProps = function(state) {
  return {
    myOfferMyItemRelations: state.myOfferMyItemRelations,
    myItemsId: state.myItemsId,
    myItemInOffer: state.myItemInOffer,
    myActiveOfferIds: state.myActiveOfferIds,
    userId: state.userId,
  };
};

const Response = connect(
  mapStateToProps,
  {},
)(function response(props) {
  return (
    <div className="response">
      <p> Offer: </p>
      <Offer id={props.offer_id} />

      <p>Item</p>
      <Item id={props.item_id} />

      <p>state: {props.state}</p>

      <p> {props.time}</p>
      {props.userId === props.user_id && props.state === 'open' && <ButtonForCancelResponse id={props.id} itemId={props.item_id} offerId={props.offer_id} />}
      {props.userId !== props.user_id && props.state === 'open' && <ButtonForAcceptResponse id={props.id} offerId={props.offer_id} />}
      {props.userId !== props.user_id && props.state === 'open' && <ButtonForRejectResponse id={props.id} offerId={props.offer_id} />}
    </div>
  );
});

export default Response;
