import React from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import { cancelResponse } from '../../../redux/actions';
import Button from '../../../components/Button';

const mapStateToProps = function(state) {
  return {
    myActiveResponse: state.myActiveResponse,
    myItemInResponse: state.myItemInResponse,
    myResponse: state.myResponse,
  };
};

const ButtonForCancelResponse = connect(
  mapStateToProps,
  { cancelResponse },
)(function BlankForCancelResponse(props) {
  function cancelResponse(id, itemId, offerId) {
    axios.post(`/api/offer/${offerId}/offer_response/${id}`).then(response => {
      //[0]
      if (response.status === 200) {
        if (response.data) {
          props.cancelResponse(id, itemId);
          alert(`ответ на предложение отменен`);
        }
      }
    });
  }

  return (
    <Button
      name="Отменить ответ!"
      action={() => {
        cancelResponse(props.id, props.itemId, props.offerId);
      }}
    />
  );
});

export default ButtonForCancelResponse;
