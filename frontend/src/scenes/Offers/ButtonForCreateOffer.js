import React from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import { login, unLogin, createOfferAction } from '../../redux/actions';
import Button from '../../components/Button';

const mapStateToProps = function(state) {
  return {
    myItemsId: state.myItemsId,
  };
};

const ButtonForCreateOffer = connect(
  mapStateToProps,
  { login, unLogin, createOfferAction },
)(function BlankButtonForCreateOffer(props) {
  function createOffer(id) {
    axios.post('/api/offer/', { item_id: id }).then(response => {
      if (response.status === 200) {
        props.createOfferAction(response.data, id);
        alert('Предмет отправлен на обмен!');
      }
    });
  }

  return (
    <Button
      name="Обменять"
      action={() => {
        createOffer(props.id);
      }}
    />
  );
});

export default ButtonForCreateOffer;
