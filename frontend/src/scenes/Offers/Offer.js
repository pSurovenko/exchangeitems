import React, { Component, useState, useEffect } from 'react';
const axios = require('axios');
import { connect } from 'react-redux';
import { login, unLogin } from '../../redux/actions';
import Item from '../../components/Item';
import FormForAnswerOffer from './FormForAnswerOffer';
// import ButtonForAddItem from "../scenes/ItemStore/ButtonForAddItem";
// import ButtonForCreateOffer from "../scenes/Offers/ButtonForCreateOffer";
import ButtonForCancelOffer from './ButtonForCancelOffer';
// import Image from './Image';

const mapStateToProps = function(state) {
  return {
    myOfferMyItemRelations: state.myOfferMyItemRelations,
    myItemsId: state.myItemsId,
    myItemInOffer: state.myItemInOffer,
    myActiveOfferIds: state.myActiveOfferIds,
    userId: state.userId,
  };
};

const Offer = connect(
  mapStateToProps,
  { login, unLogin },
)(function offer(props) {
  const [OfferData, setOfferData] = useState(null);
  const [AnswerAble, setAnswerable] = useState(false);

  useEffect(() => {
    axios
      .get('/api/offer/' + props.id.toString())
      .then(response => {
        setOfferData(response.data);
        if (response.data.state === 'open') {
          setAnswerable(true);
        }
      })
      .catch();
  }, []);

  if (OfferData === null) {
    return <div />;
  }

  return (
    <div className="Offer">
      <p> offer Id: {OfferData.id} </p>
      <p>state: {OfferData.state}</p>
      <p>date: {OfferData.time}</p>
      <p>answerable: {AnswerAble.toString()}</p>
      <p> Item: </p>
      <Item id={OfferData.item_id} answerable={false} />
      {OfferData.user_id === props.userId && props.myActiveOfferIds.indexOf(OfferData.id) !== -1 && <ButtonForCancelOffer id={OfferData.item_id} />}
    </div>
  );
});

export default Offer;
