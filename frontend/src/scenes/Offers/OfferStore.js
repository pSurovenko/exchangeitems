import React, { Component, useState, useEffect } from 'react';
import Offer from './Offer';
import { connect } from 'react-redux';
import { login, unLogin } from '../../redux/actions';
import Item from '../Bag/MyBag';
import FormForAnswerOffer from './FormForAnswerOffer';

const mapStateToProps = function(state) {
  return {
    myItemsId: state.myItemsId,
    myItemInOffer: state.myItemInOffer,
    myActiveOfferIds: state.myActiveOfferIds,
    allOffersId: state.allOffersId,
    allActiveOffer: state.allActiveOffer,
    myOffersId: state.myOffersId,
  };
};

const OfferStore = connect(
  mapStateToProps,
  {},
)(function offerStore(props) {
  const [OfferForExchangeId, setOfferForExchangeId] = useState(null);
  const [OfferList, SetOfferList] = useState(props.allOffersId);

  function choiceOffer(offerId) {
    if (props.allActiveOffer.indexOf(offerId) === -1) {
      alert('предложение закрыто!');
      return;
    }
    if (props.myOffersId.indexOf(offerId) !== -1) {
      alert('Это ваше предложение, выберите предложение другого пользователя!');
      return;
    }
    setOfferForExchangeId(offerId);
    console.log(offerId);
  }

  function changeOfferList(offerListId) {
    SetOfferList(offerListId);
  }

  return (
    <div>
      <div>Offer STORE</div>
      <div className={'navInOffer'}>
        <a
          onClick={() => {
            changeOfferList(props.allOffersId);
          }}
        >
          {' '}
          Все
        </a>
        <a
          onClick={() => {
            changeOfferList(props.allActiveOffer);
          }}
        >
          {' '}
          Активные
        </a>
        <a
          onClick={() => {
            changeOfferList(props.myOffersId);
          }}
        >
          {' '}
          Мои
        </a>
        <a
          onClick={() => {
            changeOfferList(props.myActiveOfferIds);
          }}
        >
          {' '}
          Мои Активные
        </a>
      </div>
      <div className="OfferStore">
        <div>
          {OfferList.map(id => (
            <div
              key={id}
              onClick={() => {
                choiceOffer(id);
              }}
            >
              <Offer key={id} id={id} />
            </div>
          ))}
        </div>
        <FormForAnswerOffer offerId={OfferForExchangeId} />
      </div>
    </div>
  );
});

export default OfferStore;
