import React from 'react';
import { connect } from 'react-redux';
const axios = require('axios');
import { login, unLogin, cancelMyOffer } from '../../redux/actions';
import Button from '../../components/Button';

const mapStateToProps = function(state) {
  return {
    myItemsId: state.myItemsId,
    myOfferMyItemRelations: state.myOfferMyItemRelations,
  };
};

const ButtonForCancelOffer = connect(
  mapStateToProps,
  { login, unLogin, cancelMyOffer },
)(function BlankForCancelOffer(props) {
  function cancelOffer(id) {
    let offerId = props.myOfferMyItemRelations.filter(relation => {
      return relation.itemId === id;
    });

    axios.post('/api/offer/' + offerId[0].offerId).then(response => {
      if (response.status === 200) {
        if (response.data) {
          alert(`заявка на обмен ${offerId[0].offerId} отменена`);
          props.cancelMyOffer(offerId[0].offerId, offerId[0].itemId);
        }
      }
    });
  }

  return (
    <Button
      name="Отменить предложение обмена!"
      action={() => {
        cancelOffer(props.id);
      }}
    />
  );
});

export default ButtonForCancelOffer;
