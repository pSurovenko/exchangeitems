import React, { Component, useState, useEffect } from 'react';
const axios = require('axios');
import Item from '../../components/Item';
import { connect } from 'react-redux';
import { login, unLogin } from '../../redux/actions';

const mapStateToProps = function(state) {
  return {
    bagData: state.myItemsId,
  };
};

const MyBag = connect(
  mapStateToProps,
  {},
)(function myBag(props) {
  if (props.bagData === null) {
    return <div> you have no things!</div>;
  }

  return (
    <div className="Bag">
      <p> my bag:</p>
      {props.bagData.map(id => (
        <div key={id}>
          <Item id={id} answerable={true} />
        </div>
      ))}
      <div />
    </div>
  );
});

export default MyBag;
