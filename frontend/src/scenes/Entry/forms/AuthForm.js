import React, { Component } from 'react';
import { Redirect } from '@reach/router';
const axios = require('axios');
import { connect } from 'react-redux';
import store from '../../../redux/store';
import { login, unLogin } from '../../../redux/actions';

class BlankAuthForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    axios
      .post('/api/login', {
        email: this.state.email,
        password: this.state.password,
      })
      .then(response => {
        if (response.data.id) {
          axios.get('/api/user/my_bag').then(bagResponse => {
            let myItemsId = [];
            bagResponse.data.forEach(bagItem => {
              myItemsId.push(bagItem.item_id);
            });
            this.props.login(response.data.id, response.data.firstName, response.data.lastName, myItemsId);
          });
        } else {
          alert(response.data);
        }
      });
    event.preventDefault();
  }

  render() {
    if (this.props.logged) {
      return <Redirect to="me" />;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Email:
          <input name="email" type="email" value={this.state.email} onChange={this.handleChange} />
        </label>
        <label>
          Password:
          <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Войти" />
      </form>
    );
  }
}
const mapStateToProps = function(state) {
  return {
    logged: state.logged,
  };
};
// const mapDispatchToProps = function (dispatch) {
//     return {
//         login: ()=>{
//             dispatch(login())
//         }
//     }
// };
const AuthForm = connect(
  mapStateToProps,
  { login },
)(BlankAuthForm);
export default AuthForm;
