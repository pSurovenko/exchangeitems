import React, { Component } from 'react';
import { Redirect } from '@reach/router';
const axios = require('axios');
import { connect } from 'react-redux';
import { login, unLogin } from '../../../redux/actions';

const emptyItemsIdArray = [];

class BlankRegForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { lastName: '', firstName: '', email: '', password: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    axios
      .post('/api/signup', {
        firstname: this.state.firstName,
        lastname: this.state.lastName,
        email: this.state.email,
        password: this.state.password,
      })
      .then(response => {
        if (!isNaN(response.data)) {
          axios.post('/api/login', { email: this.state.email, password: this.state.password }).then(response => {
            this.props.login(response.data.id, response.data.firstName, response.data.lastName, emptyItemsIdArray);
          });
        } else {
          alert(response.data);
        }
      });
    event.preventDefault();
  }

  render() {
    if (this.props.logged) {
      return <Redirect to="me" />;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          firstName:
          <input name="firstName" type="text" value={this.state.firstName} onChange={this.handleChange} />
        </label>
        <label>
          lastName:
          <input name="lastName" type="text" value={this.state.lastName} onChange={this.handleChange} />
        </label>
        <label>
          Email:
          <input name="email" type="email" value={this.state.email} onChange={this.handleChange} />
        </label>
        <label>
          Password:
          <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Зарегистрироваться" />
      </form>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    logged: state.logged,
  };
};

const RegForm = connect(
  mapStateToProps,
  { login },
)(BlankRegForm);
export default RegForm;
