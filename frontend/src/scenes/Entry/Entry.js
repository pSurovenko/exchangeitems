import React, { Component, useState, useEffect } from 'react';
import AuthForm from './forms/AuthForm';
import RegForm from './forms/RegForm';

import Button from '../../components/Button';
const axios = require('axios');
const inviteForRegistration = 'Еще не с нами?';
const buttonTextForGetRegistration = 'Зарегистрироваться!';
const inviteForAuthorization = 'Уже есть аккаунт?';
const buttonTextForGetLogin = 'Авторизация!';

function Entry(props) {
  const [authReg, setAuthReg] = useState(<AuthForm />);
  const [invite, setInvite] = useState(inviteForRegistration);
  const [buttonText, setButtonText] = useState(buttonTextForGetRegistration);

  function changeAction() {
    switch (authReg.type.displayName) {
      case 'Connect(BlankAuthForm)':
        setButtonText(buttonTextForGetLogin);
        setInvite(inviteForAuthorization);
        setAuthReg(<RegForm />);
        break;
      case 'Connect(BlankRegForm)':
        setButtonText(buttonTextForGetRegistration);
        setInvite(inviteForRegistration);
        setAuthReg(<AuthForm />);
        break;
    }
  }

  let AuthRegForms = (
    <div>
      {authReg}
      <p> {invite}</p>
      <Button
        name={buttonText}
        action={() => {
          changeAction();
        }}
      />
    </div>
  );

  return AuthRegForms;
}

export default Entry;
