'use strict';

const bCrypt = require('bcrypt-nodejs');
const passport = require('koa-passport');
const LocalStrategy = require('passport-local');
const models = require('../models');

const options = {
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true, // allows us to pass back the entire request to the callback
};

passport.use('local', new LocalStrategy(options, (req, email, password, done) => {
  const isValidPassword = function (userpass, password) {
    return bCrypt.compareSync(password, userpass);
  };

  models.user.findOne({ where: { email } }).then((user) => {
    if (!user) {
      return done(null, false, { message: 'Email does not exist' });
    }
    if (!isValidPassword(user.passwd, password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }
    const userinfo = {
      id: user.id,
      email: user.email,
      firstName: user.first_name,
      lastName: user.last_name,
    };

    return done(null, userinfo);
  }).catch((err) => {
    return done(null, false, { message: 'Something went wrong with your Signin' });
  });
}));

passport.use('signup', new LocalStrategy(options, (req, email, password, done) => {
  const generateHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
  };

  models.user.findOne({ where: { email } })
    .then((user) => { //eslint-disable-line
      if (user) {
        return done(null, false, { message: 'this email has already been used' });
      }
      const userPassword = generateHash(password);
      const data = {
        first_name: req.body.firstname,
        last_name: req.body.lastname,
        email,
        passwd: userPassword,
      };

      models.user.create(data).then((newUser) => { //eslint-disable-line
        if (!newUser) {
          return done(null, false);
        }
        if (newUser) {
          return done(null, newUser);
        }
      });
    });
}));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((userId, done) => {
  models.user.findById(
    userId,
  ).then((user) => {
    done(null, {
      id: userId,
      email: user.get('email'),
      firstName: user.get('first_name'),
      lastName: user.get('last_name'),
    });
  });
});

module.exports = passport;

