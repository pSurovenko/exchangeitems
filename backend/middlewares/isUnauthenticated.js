'use strict';

const Router = require('koa-router'); //eslint-disable-line
const passport = require('./passport'); //eslint-disable-line

module.exports = (ctx, next) => { //eslint-disable-line
  if (ctx.isUnauthenticated()) {
    ctx.throw(401, 'Unauthenticated');
  } else {
    return next();
  }
};
