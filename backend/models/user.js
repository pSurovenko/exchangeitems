'use strict';

module.exports = function (sequelize, Sequelize) {
  const User = sequelize.define('user', {
    first_name: {
      type: Sequelize.STRING,
      notEmpty: true,
    },
    last_name: {
      type: Sequelize.STRING,
      notEmpty: true,
    },
    email: {
      type: Sequelize.STRING,
      validate: {
        isEmail: true,
      },
    },
    passwd: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  User.associate = function (models) {
    User.belongsToMany(models.item, {
      through: models.bag,
    });
  };

  return User;
};
