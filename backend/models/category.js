'use strict';

module.exports = function (sequelize, Sequelize) {
  const Category = sequelize.define('category', {
    name: {
      type: Sequelize.STRING,
      notEmpty: true,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  return Category;
};
