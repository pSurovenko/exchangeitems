'use strict';

module.exports = function (sequelize, Sequelize) {
  const Image = sequelize.define('image', {
    src: {
      type: Sequelize.STRING,
      notEmpty: true,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  // Image.associate = function (models) {
  //     Image.belongsTo(models['item'], {
  //         constraints: false
  //     });
  // };

  return Image;
};
