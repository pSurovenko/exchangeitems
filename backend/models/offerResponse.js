'use strict';

module.exports = function (sequelize, Sequelize) {
  const OfferResponse = sequelize.define('response', {
    user_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
      allowNull: false,
    },
    item_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
      allowNull: false,
    },
    state: {
      type: Sequelize.STRING,
      notEmpty: true,
      allowNull: false,
    },
    offer_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
      allowNull: false,
    },
    time: {
      type: Sequelize.DATE,
      notEmpty: true,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  OfferResponse.associate = function (models) {
    OfferResponse.belongsTo(models.user);
    OfferResponse.belongsTo(models.item);
    OfferResponse.belongsTo(models.offer);
  };

  return OfferResponse;
};
