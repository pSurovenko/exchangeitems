'use strict';

module.exports = function (sequelize, Sequelize) {
  const ItemName = sequelize.define('name', {
    content: {
      type: Sequelize.STRING,
      notEmpty: true,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  // ItemName.associate = function (models) {
  //     ItemName.belongTo(models['item']);
  // };

  return ItemName;
};
