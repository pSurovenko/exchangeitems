'use strict';

module.exports = function (sequelize, Sequelize) {
  const Item = sequelize.define('item', {
    main_img_id: {
      type: Sequelize.INTEGER,

    },
    category_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
    },
    name_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
      allowNull: false,
    },
    description_id: {
      type: Sequelize.INTEGER,

      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  Item.associate = function (models) {
    Item.belongsToMany(models.user, {
      through: models.bag,
    });
    // Items.belongsTo(models['image'], {
    //     as: 'main_image',
    //     foreignKey: 'main_img_id',
    //     constraints: false
    // });
    Item.belongsTo(models.category);
    Item.belongsTo(models.name);
    Item.belongsTo(models.description);
    Item.belongsTo(models.image);
  };

  return Item;
};
