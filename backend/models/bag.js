'use strict';

module.exports = function (sequelize, Sequelize) {
  const Bag = sequelize.define('bag', {
    user_id: {
      type: Sequelize.INTEGER,
      notEmpty: true,
      allowNull: false,
    },
    item_id: {
      type: Sequelize.INTEGER,
      notEmpty: true,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  return Bag;
};
