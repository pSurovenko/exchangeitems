'use strict';

module.exports = function (sequelize, Sequelize) {
  const Offer = sequelize.define('offer', {
    user_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
      allowNull: false,
    },
    item_id: {
      type: Sequelize.INTEGER,

      notEmpty: true,
      allowNull: false,
    },
    state: {
      type: Sequelize.STRING,
      notEmpty: true,
      allowNull: false,
    },
    time: {
      type: Sequelize.DATE,
      notEmpty: true,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  Offer.associate = function (models) {
    Offer.belongsTo(models.user);
    Offer.belongsTo(models.item);
    Offer.hasMany(models.response);
  };

  return Offer;
};
