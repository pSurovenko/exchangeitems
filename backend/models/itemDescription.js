'use strict';

module.exports = function (sequelize, Sequelize) {
  const ItemDescription = sequelize.define('description', {
    content: {
      type: Sequelize.STRING,
      notEmpty: true,
      allowNull: false,
    },
  }, {
    timestamps: false,
    underscored: true,
    freezeTableName: true,
  });

  // ItemDescription.associate = function (models) {
  //     ItemDescription.belongsTo(models['item']);
  // };

  return ItemDescription;
};
