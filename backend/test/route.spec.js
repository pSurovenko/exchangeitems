'use strict';

const request = require('supertest');
const app = require('../app');
const sequelize = require('../models').sequelize;

describe('Router', () => {
  beforeAll(async () => {
    this.app = app.callback();
  });
  afterAll(async () => {
    await sequelize.close();
  });

  describe('GET /me', () => {
    test('when not authenticated', async () => {
      const response = await request(this.app)
        .get('/me');

      expect(response.statusCode).toBe(401);
      expect(response.text).toBe('Unauthenticated');
    });

    test('when authenticated', async () => {
      // request.agent allows retaining cookies with each request
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      const response = await agent
        .get('/me');

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual(
        {
          id: 4,
          email: 'test3@mail.com',
          firstName: 'test3',
          lastName: 'test3',
        },

      );
    });
  });

  describe('login, logout, signup route', () => {
    test('/login POST, good request ', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .post('/login').send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual(
        {
          id: 4,
          email: 'test3@mail.com',
          firstName: 'test3',
          lastName: 'test3',
        },

      );
    });
    test('/login POST, bad request ', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .post('/login').send({
          email: 'test3@mail.com',
          password: '3test',
        });

      expect(response.statusCode).toBe(400);

      expect(response.text).toEqual('Incorrect login/password');
    });

    test('/logout POST', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      const response = await agent.post('/logout');

      expect(response.statusCode).toBe(200);
      expect(response.text).toEqual('logout');
    });
  });

  describe('item router', () => {
    test('/item POST', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .post('/item')
        .send(
          {
            itemDescription: '1',
            itemName: '1',
            main_img_id: '1',
            category_id: '1',
          },
        );

      expect(response.statusCode).toBe(200);
      expect(response.text).toEqual('ok');
    });

    test('/item GET', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/item')
        .send(
          {
            itemDescription: '1',
            itemName: '1',
            main_img_id: '1',
            category_id: '1',
          },
        );
      const response = await agent.get('/item');

      expect(response.statusCode).toBe(200);
      expect(!isNaN(Number.parseInt(response.body[0].id))).toEqual(true);
    });

    test('/item/:id GET', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/item')
        .send(
          {
            itemDescription: '1',
            itemName: '1',
            main_img_id: '1',
            category_id: '1',
          },
        );
      const response = await agent.get('/item/16');

      expect(response.statusCode).toBe(200);
      expect(typeof response.body.id).toEqual('number');
      expect(typeof response.body.image_id).toEqual('number');
      expect(typeof response.body.category).toEqual('object');
      expect(typeof response.body.category.name).toEqual('string');
      expect(typeof response.body.name).toEqual('object');
      expect(typeof response.body.name.content).toEqual('string');
      expect(typeof response.body.description).toEqual('object');
      expect(typeof response.body.description.content).toEqual('string');
    });

    test('/item/name GET', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/item')
        .send(
          {
            itemDescription: '1',
            itemName: '1',
            main_img_id: '1',
            category_id: '1',
          },
        );
      const response = await agent.get('/item/name');

      expect(response.statusCode).toBe(200);
      expect(typeof response.body[0].id).toEqual('number');
      expect(typeof response.body[0].content).toEqual('string');
    });

    test('/item/find POST', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/item')
        .send(
          {
            itemDescription: '1',
            itemName: '1',
            main_img_id: '1',
            category_id: '1',
          },
        );

      const response = await agent
        .post('/item/find')
        .send({
          category_id: '1',
        });

      expect(response.statusCode).toBe(200);
      expect(typeof response.body[0]).toEqual('object');
    });
  });

  describe('image router', () => {
    test('image/:id GET', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .get('/image/2');

      expect(response.statusCode).toBe(200);
      expect(typeof response.text).toEqual('string');
    });
  });

  describe('user router', () => {
    test('user/:id GET', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .get('/user/2');

      expect(response.statusCode).toBe(200);
      expect(typeof response.body).toEqual('object');
      expect(response.body).toHaveProperty('id');
      expect(response.body).toHaveProperty('first_name');
      expect(response.body).toHaveProperty('last_name');
      expect(response.body).toHaveProperty('email');
    });

    test('when not authenticated, user/my_bag GET ', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .get('/user/my_bag');

      expect(response.statusCode).toBe(401);
      expect(response.text).toBe('Unauthenticated');
    });

    test('when authenticated, user/my_bag POST ', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      const response = await agent
        .post('/user/my_bag')
        .send({
          item_id: '16',
        });

      expect(response.statusCode).toBe(200);
      expect(response.text).toEqual('ok');
    });

    test('when authenticated, user/my_bag GET ', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      await agent
        .post('/user/my_bag')
        .send({
          item_id: '16',
        });

      const response = await agent
        .get('/user/my_bag');

      expect(response.statusCode).toBe(200);
      expect(response.body[0]).toHaveProperty('id');
      expect(response.body[0]).toHaveProperty('user_id');
      expect(response.body[0]).toHaveProperty('item_id');
    });
  });

  describe('offer router', () => {
    test('/offer, GET', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .get('/offer');

      expect(response.statusCode).toBe(200);
      expect(response.body[0]).toHaveProperty('state');
      expect(response.body[0]).toHaveProperty('user_id');
      expect(response.body[0]).toHaveProperty('item_id');
      expect(response.body[0]).toHaveProperty('state');
      expect(response.body[0]).toHaveProperty('time');
    });

    test('when not authenticated /offer, POST', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .post('/offer');

      expect(response.statusCode).toBe(401);
    });

    test('when authenticated /offer, POST', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      const response = await agent
        .post('/offer').send({
          item_id: '16',
        });

      expect(response.statusCode).toBe(200);
      expect(response.body).not.toBeNaN();
    });

    test('when not authenticated /offer/my, GET', async () => {
      const agent = await request.agent(this.app);

      const response = await agent
        .get('/offer/my');

      expect(response.statusCode).toBe(401);
      expect(response.text).toEqual('Unauthenticated');
    });

    test('when authenticated /offer/my, GET', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      const response = await agent
        .get('/offer/my');

      expect(response.statusCode).toBe(200 || 204);

      expect(response.body[0].user_id).toBe(4);
    });

    test('when authenticated /offer/:id, POST (canceling offer)', async () => {
      const agent = await request.agent(this.app);

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });
      const myOffers = await agent
        .get('/offer/my');
      const myPenultOfferId = myOffers.body[myOffers.body.length - 1].id;
      const response = await agent
        .post(`/offer/${myPenultOfferId}`);

      expect(response.statusCode).toBe(200);
      // expect(response.body).toEqual(true);
      expect(typeof response.body).toEqual('boolean');
    });
  });

  describe('response offer router. cancel, accept, reject. exchange items', () => {
    test('first user:(enter, add item, to issue 3 offer, logout),'
          + ' second user:(enter, add item, to issue 3 response, cancel 1 response, logout) '
          + 'first user:(enter, reject 1 response, accept 1 response)',
    async () => {
      const agent = await request.agent(this.app);
      const offersId = [];
      const responseId = [];

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      await agent
        .post('user/my_bag')
        .send({
          item_id: '16',
        });
      offersId.push(await agent
        .post('/offer')
        .send({
          item_id: '16',
        }).text);

      await agent
        .post('user/my_bag')
        .send({
          item_id: '17',
        });
      offersId.push(await agent
        .post('/offer')
        .send({
          item_id: '17',
        }).text);

      await agent
        .post('user/my_bag')
        .send({
          item_id: '18',
        });
      offersId.push(await agent
        .post('/offer')
        .send({
          item_id: '18',
        }).text);

      await agent
        .post('/logout');

      await agent
        .post('/login')
        .send({
          email: 'test4@mail.com',
          password: 'test4',
        });

      await agent
        .post('user/my_bag')
        .send({
          item_id: '19',
        });
      responseId.push(await agent
        .post(`offer/${offersId[0]}/`)
        .send({
          item_id: '19',
        }).text);

      responseId.push(await agent
        .post(`offer/${offersId[1]}/`)
        .send({
          item_id: '19',
        }).text);

      responseId.push(await agent
        .post(`offer/${offersId[2]}/`)
        .send({
          item_id: '19',
        }).text);

      // canceling response
      await agent
        .post(`offer/${offersId[2]}/${responseId[0]}`);

      await agent
        .post('/logout');

      await agent
        .post('/login')
        .send({
          email: 'test3@mail.com',
          password: 'test3',
        });

      await agent
        .post(`offer/${offersId[2]}/${responseId[1]}`)
        .send({
          action: 'accept',
        });
      const response = await agent
        .get(`offer/${offersId[2]}/${responseId[1]}`);

      expect(response.statusCode).toBe(200);
      expect(response.body.state).toEqual('accepted');

      await agent
        .post(`offer/${offersId[2]}/${responseId[2]}`)
        .send({
          action: 'reject',
        });
      const response2 = await agent
        .get(`offer/${offersId[2]}/${responseId[2]}`);

      expect(response2.statusCode).toBe(200);
      expect(response2.body.state).toEqual('rejected');
    });
  });
});
