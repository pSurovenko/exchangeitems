'use strict';

const path = require('path');
const Koa = require('koa');
const serve = require('koa-static');
const koaBody = require('koa-body');
const bunyan = require('bunyan');
const koaLogger = require('koa-bunyan');
const env = require('dotenv').load(); //eslint-disable-line
const session = require('koa-generic-session');
const send = require('koa-send');
const SequelizeSessionStore = require('koa-generic-session-sequelize');
const passport = require('./middlewares/passport');
const router = require('./routers');
const models = require('./models');

// const port = process.env.PORT || PORT;
const app = new Koa();
const logger = bunyan.createLogger({ name: 'app' });

if (process.env.NODE_ENV === 'test') {
  logger.level(bunyan.ERROR);
}

app.context.log = logger;
app.use(koaBody({
  formidable: {
    uploadDir: './backend/upload',
    keepExtensions: true,
    maxFileSize: 1024 * 1024 * 10,
  }, // This is where the files would come
  multipart: true,
  urlencoded: true,
}));
app.use(serve('public'));

app.use(koaLogger(logger));
// app.use(koaBody());

app.keys = ['secret'];

models.sequelize.sync().then(() => {
  console.log('ok'); //eslint-disable-line
}).catch((err) => {
  console.log(err, 'Something went wrong with the Database Update!'); //eslint-disable-line
});

app.use(session({
  store: new SequelizeSessionStore(
    models.sequelize, {
      tableName: 'sessions',
    },
  ),
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(router.routes());
app.use(router.allowedMethods());

app.use(async (ctx) => {
  const _path = path.resolve('public');

  await send(ctx, 'index.html', { root: _path });
});

// app.listen(port, () => {
//   logger.info(`Server is started on ${port} port`);
// });
module.exports = app;
