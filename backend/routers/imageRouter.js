'use strict';

const Router = require('koa-router');

const router = new Router({ prefix: '/image' });
const fs = require('fs');
const path = require('path');
const models = require('.././models');

router
  .get('/:id', async (ctx, next) => {   // eslint-disable-line
      if (isNaN(ctx.params.id)) {         // eslint-disable-line
      return next();
    }

    await models.image.findById(
      ctx.params.id,
      // {attributes: ['src']},
    )
      .then((img) => {
        // ctx.type =
        ctx.status = 200;
        ctx.body = img.src;
      });
  })
  .post('/', async (ctx) => {
    const dataForCreateImage = {
      src: ctx.request.body.src,
    };
    let ImageId;

    await models.image.create(dataForCreateImage).then((image) => {
      ImageId = image.id;
    });
    ctx.body = ImageId;
    ctx.status = 200;
  })
  .post('/upload', async (ctx) => {
    const file = ctx.request.files.fileUpload.path;

    const folderForSaveImage = 'public/img/';
    const fileName = new Date().getTime() + path.extname(ctx.request.files.fileUpload.name);
    const newFileInStatic = folderForSaveImage + fileName;

    fs.createReadStream(file).pipe(fs.createWriteStream(newFileInStatic));
    fs.unlink(file, () => {

    });
    ctx.status = 200;
    ctx.body = fileName;
  });

module.exports = router;
