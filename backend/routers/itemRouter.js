'use strict';

const Router = require('koa-router');
const models = require('.././models');

const router = new Router({ prefix: '/item' });

router
  .get('/', async (ctx) => {
    //    get all items
    let items;

    await models.item.findAll({
      attributes: ['id', ['main_img_id', 'image_id']],
      include: [{
        model: models.category,
        attributes: ['name'],
        where: { id: models.Sequelize.col('item.category_id') },
      },
      {
        model: models.name,
        attributes: ['content'],
        where: { id: models.Sequelize.col('item.name_id') },
      },
      {
        model: models.description,
        attributes: ['content'],
        where: { id: models.Sequelize.col('item.description_id') },
      }],
    }).then((foundItems) => {
      items = foundItems;
    }).catch(err => ctx.throw(401, err.message));

    ctx.status = 200;
    ctx.body = items;
  })

  .get('/:id', async (ctx, next) => { // eslint-disable-line
    if (isNaN(ctx.params.id)) { // eslint-disable-line
      return next();
    }
    //    get info about item by id
    let item;

    await models.item.findById(ctx.params.id,
      {
        attributes: ['id', ['main_img_id', 'image_id']],
        include: [{
          model: models.category,
          attributes: ['name'],
          where: { id: models.Sequelize.col('item.category_id') },
        },
        {
          model: models.name,
          attributes: ['content'],
          where: { id: models.Sequelize.col('item.name_id') },
        },
        {
          model: models.description,
          attributes: ['content'],
          where: { id: models.Sequelize.col('item.description_id') },
        }],
      }).then((items) => {
      item = items;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = item;
  })

  .get('/ids', async (ctx) => {
    //    get all items
    const items = [];

    await models.item.findAll({ attributes: ['id'] })
      .then((foundItems) => {
        foundItems.forEach((item) => {
          items.push(item.id);
        });
        // items = foundItems;
      }).catch(err => ctx.throw(401, err.message));

    ctx.status = 200;
    ctx.body = items;
  })

  .post('/', async (ctx) => {
    //    create new item
    // to do create main_img_id, and creating image;
    let itemNameId; let itemDescriptionId;
    // let  main_img_id;
    // let main_img_id = null;

    await models.description.create({ content: ctx.request.body.itemDescription }).then((itemDescription) => {
      itemDescriptionId = itemDescription.get('id');
    }).catch(err => ctx.throw(401, err.message));

    await models.name.create({ content: ctx.request.body.itemName }).then((itemName) => {
      itemNameId = itemName.get('id');
    }).catch(err => ctx.throw(401, err.message));

    const dataForCreateItem = {
      main_img_id: ctx.request.body.main_img_id,
      category_id: ctx.request.body.category_id,
      name_id: itemNameId,
      description_id: itemDescriptionId,
    };

    await models.item.create(dataForCreateItem)
      .then(() => {
        ctx.status = 200;
        ctx.body = 'ok';
        // ctx.redirect('/item', ctx);
      })
      .catch(err => ctx.throw(401, err.message));
  })

  .get('/category', async (ctx) => {
    // get all category
    let allCategory;

    await models.category.findAll().then((category) => {
      allCategory = category;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = allCategory;
  })

  .get('/category/:id', async (ctx) => {
    //    get info about category by id
    let searchedCategory;

    await models.category.findById(ctx.params.id).then((category) => {
      searchedCategory = category;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = searchedCategory;
  })

  .get('/name', async (ctx) => {
    let allName;

    await models.name.findAll().then((names) => {
      allName = names;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = allName;
  })

  .post('/find', async (ctx) => {
    let searchedItems;

    await models.item.findAll({
      where:
        ctx.request.body,

      attributes: ['id', ['main_img_id', 'image_id']],
      include: [{
        model: models.category,
        attributes: ['name'],
        where: { id: models.Sequelize.col('item.category_id') },
      },
      {
        model: models.name,
        attributes: ['content'],
        where: { id: models.Sequelize.col('item.name_id') },
      },
      {
        model: models.description,
        attributes: ['content'],
        where: { id: models.Sequelize.col('item.description_id') },
      }],
    }).then((items) => {
      searchedItems = items;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = searchedItems;
  });

module.exports = router;
