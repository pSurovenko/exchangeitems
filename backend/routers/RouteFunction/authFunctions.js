'use strict';

const passport = require('../../middlewares/passport');

module.exports.me = (ctx) => {
  ctx.status = 200;
  ctx.body = ctx.state.user;
};

module.exports.login = async (ctx) => {
  if (ctx.isAuthenticated()) {
    ctx.message = 'You are authenticated already';

    return this.root(ctx);
  }
  await passport.authenticate('local', {}, async (err, user) => {
    if (!user) {
      ctx.throw(400, 'Incorrect login/password');
    }
    ctx.login(user, (err) => {
      if (err) {
        ctx.throw(400, err.message);
      }
      ctx.status = 200;
      ctx.body = user;
    });
  })(ctx);
};

module.exports.logout = async (ctx) => {
  await ctx.logout();
  ctx.status = 200;
  ctx.message = 'You are logout';
  ctx.body = 'logout';
};

module.exports.signup = async (ctx) => {
  if (ctx.isAuthenticated()) {
    ctx.body = 'You are authenticated already';

    return ctx;
  }
  await passport.authenticate('signup', (err, user, message) => {
    ctx.body = err || message || user.id;

    return ctx;
  })(ctx);
};

module.exports.root = async (ctx) => {
  ctx.body = ctx.message;
};
