'use strict';

const Router = require('koa-router');

const router = new Router({ prefix: '/user' });
const models = require('.././models');
const passport = require('../middlewares/passport'); // eslint-disable-line

router
  .get('/', async (ctx) => {
    await models.user.findAll({
      attributes: ['id', 'first_name', 'last_name', 'email'],
    }).then((users) => {
      ctx.status = 200;
      ctx.body = users;
    });
  })
  .get('/:id', async (ctx, next) => {
    //  get user by id
    if (isNaN(ctx.params.id)) {
      return next();
    }
    await models.user.findById(ctx.params.id, {
      attributes: ['id', 'first_name', 'last_name', 'email'],
    }).then((user) => {
      ctx.status = 200;
      ctx.body = user;
    });
  })
// .get('/:id/about', async (ctx)=> {
//     ctx.statys = 200;
//     ctx.body= `get about user id = ${ctx.params.id}`;
// //    get about user
// } )
// .put('/:id/about', async (ctx) => {
//     ctx.statys = 200;
//     ctx.body= `upDate user info POST id =  ${ctx.params.id}`;
// } )
  .get('/:id/bag', async (ctx) => {
    //    get user's bug
    await models.bag.findAll({ where: { user_id: ctx.params.id } }).then((bag) => {
      ctx.status = 200;
      ctx.body = bag;
    });
  })
  .get('/my_bag', async (ctx) => {
    if (ctx.isUnauthenticated()) {
      ctx.throw(401, 'Unauthenticated');
    }
    await models.bag.findAll(
      {
        where: { user_id: ctx.state.user.id },
        attributes: ['id', 'user_id', 'item_id'],
      },
    )
      .then((bag) => {
        ctx.status = 200;
        ctx.body = bag;
      }).catch(err => ctx.throw(500, err.message));
  })

  .post('/my_bag', async (ctx) => { // add item to user's bag
    if (ctx.isUnauthenticated()) {
      ctx.throw(401, 'Unauthenticated');
    }
    const dataForRequest = {
      user_id: ctx.state.user.id,
      item_id: parseInt(ctx.request.body.item_id, 10),
    };

    await models.bag.create(dataForRequest).then(() => {
      ctx.status = 200;
      ctx.body = 'ok';
    });
  });
// .get('/:id/bag/:id_bag', async (ctx) => {
//     ctx.status = 301;
//     ctx.redirect(`/item/${ctx.params.id_bag}`)
// } )
// .get('/:id/offer', async (ctx) => {
//     ctx.status = 200;
//     ctx.body = `get user's offer id = ${ctx.params.id}`;
// //    get user's offer
// } )
// .get('/:id/offer/:id_offer', async (ctx) => {
//     ctx.status = 301;
//     ctx.redirect(`/offer/${ctx.params.id_offer}`)
// })

module.exports = router;
