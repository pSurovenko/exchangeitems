'use strict';

const Router = require('koa-router');
const authRouter = require('./auth');
const itemRouter = require('./itemRouter');
const offerRouter = require('./offerRouter');
const userRouter = require('./usersRouter');
const imageRouter = require('./imageRouter');

const router = new Router({ prefix: '/api' });

router.use('', itemRouter.routes());
router.use('', offerRouter.routes());
router.use('', userRouter.routes());
router.use('', imageRouter.routes());
router.use('', authRouter.routes());

module.exports = router;
