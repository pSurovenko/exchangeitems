'use strict';

const Router = require('koa-router');

const router = new Router({ prefix: '/offer' });
const offerResponseRouter = require('./offerResponseRouter');
const models = require('.././models');
const passport = require('../middlewares/passport'); // eslint-disable-line
const isUnAuth = require('../middlewares/isUnauthenticated'); // eslint-disable-line

const stateForCreateOffer = 'open';
const stateCanceledOffer = 'cancel';
const stateRejectedResponse = 'Rejected';

router
  .get('/', async (ctx) => {
    //    get all offer
    let allOffers;

    await models.offer.findAll().then((offers) => {
      allOffers = offers;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = allOffers;
  })
  .get('/ids', async (ctx, next) => {
    let allOffers;

    await models.offer.findAll({ attributes: ['id'] }).then((offers) => {
      allOffers = offers;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = allOffers;
  })
  .get('/:id', async (ctx, next) => {
    if (isNaN(ctx.params.id)) {
      return next();
    }
    //    get offer info by id
    let searchedOffer;

    await models.offer.findById(ctx.params.id).then((offer) => {
      searchedOffer = offer;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = searchedOffer;
  })
  .post('/:id', async (ctx, next) => {
    if (isNaN(ctx.params.id)) {
      return next();
    }
    let stateCanceled;

    await models.offer.findById(ctx.params.id).then((offer) => {
      if (offer.user_id === ctx.state.user.id) {
        offer.state = stateCanceledOffer;
        stateCanceled = true;
        models.response.findAll({
          where: {
            offer_id: ctx.params.id,
            state: stateForCreateOffer,
          },
        })
          .then((answers) => {
            for (const answer of answers) {   // eslint-disable-line
              answer.state = stateRejectedResponse;
              answer.save();
            }
            answers.save;                   // eslint-disable-line
          });
      } else {
        stateCanceled = false;
      }

      ctx.status = 200;
      ctx.body = stateCanceled;

      return offer.save();
    })
      .catch(err => ctx.throw(401, err.message));
  })
  .post('/find', async (ctx) => {
    // find response
    let searchedOffers;

    await models.offer.findAll({
      where:
                ctx.request.body,
    }).then((offers) => {
      searchedOffers = offers;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = searchedOffers;
  })
  .get('/my_response', async (ctx) => {
    await models.response.findAll({ where: { user_id: ctx.state.user.id } })
      .then((offers_response) => {
        ctx.status = 200;
        ctx.body = offers_response;
      }).catch(err => ctx.throw(401, err.message));
  })
  // .use(isUnAuth)
  .post('/', async (ctx) => {
    //    create new offer
    if (ctx.isUnauthenticated()) {
      ctx.throw(401, 'Unauthenticated');
    }
    const dataForOffer = {
      user_id: ctx.state.user.id,
      item_id: ctx.request.body.item_id,
      state: stateForCreateOffer,
      time: new Date().toLocaleDateString(),
    };

    await models.offer.create(dataForOffer).then((offer) => {
      ctx.status = 200;
      ctx.body = offer.id;
    }).catch(err => ctx.throw(400, err.message));
  })
  .get('/my', async (ctx) => {
    if (ctx.isUnauthenticated()) {
      ctx.throw(401, 'Unauthenticated');
    }
    let myOffers;

    await models.offer.findAll({
      where: {
        user_id: ctx.state.user.id,
      },
    }).then((offers) => {
      myOffers = offers;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = myOffers;
  });

router.use('/:id', offerResponseRouter.routes(), offerResponseRouter.allowedMethods());

module.exports = router;
