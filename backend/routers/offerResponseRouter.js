'use strict';

const Router = require('koa-router');

const router = new Router({ prefix: '/offer_response' });
const models = require('.././models');
const isUnAuth = require('../middlewares/isUnauthenticated');

const stateForCreateOfferResponse = 'open';
const stateResolveOfferResponse = 'resolve';
const stateRejectOfferResponse = 'reject';

async function exchangeItems(offer1, offer2, ctx) {
  await models.bag
    .findAll({
      where: {
        user_id: offer1.user_id,
        item_id: offer1.item_id,
      },
    })
    .then((bag) => {
      bag[0].user_id = offer2.user_id;
      bag[0].save();
      bag.save;                        // eslint-disable-line
    }).catch(err => ctx.throw(401, err.message));
}

router
  .get('/', async (ctx) => {
    //    get all responses by offer id
    let allOffersResponses;

    await models.response.findAll({
      where: {
        offer_id: ctx.params.id,
      },
    }).then((responses) => {
      allOffersResponses = responses;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = allOffersResponses;
  })
  .get('/:id_response', async (ctx, next) => {
    if (isNaN(ctx.params.id_response)) {
      return next();
    }
    //    get info about offer_response
    let searchedOffersResponses;

    await models.response.findAll({
      where: {
        offer_id: ctx.params.id,
        id: ctx.params.id_response,
      },
    }).then((response) => {
      searchedOffersResponses = response;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = searchedOffersResponses;
  })
  .post('/:id_response', async (ctx, next) => {
    if (isNaN(ctx.params.id_response)) {
      return next();
    }
    //    check ability and
    //    resolve, reject or censel response
    let searchedOffer;

    await models.offer.findById(ctx.params.id).then((offer) => {
      if (offer.state === stateForCreateOfferResponse) {
        searchedOffer = offer;
      } else {
        ctx.status = 203;
        ctx.body = 'offer is not open';
      }
    }).catch(err => ctx.throw(401, err.message));

    await models.response.findById(ctx.params.id_response).then((offer_response) => {
      if (offer_response.state === stateForCreateOfferResponse) {
        if (ctx.state.user.id === searchedOffer.user_id) {
          if (ctx.request.body.action === 'accept') {
            searchedOffer.state = stateResolveOfferResponse;
            offer_response.state = stateResolveOfferResponse;
            // exchange items:
            exchangeItems(searchedOffer, offer_response, ctx);
            exchangeItems(offer_response, searchedOffer, ctx);
            searchedOffer.save();
            offer_response.save();
            ctx.status = 200;
            ctx.body = 'ok';
          }
          if (ctx.request.body.action === stateRejectOfferResponse) {
            offer_response.state = 'rejected';
            offer_response.save();
            ctx.status = 200;
            ctx.body = 'ok';
          }
        }

        if (ctx.state.user.id === offer_response.user_id) {
          offer_response.state = 'Canceled';
          offer_response.save();
          ctx.status = 200;
          ctx.body = 'ok';
        }
      }
    }).catch(err => ctx.throw(401, err.message));
  })
  .get('/my', async (ctx) => {
    let myOffersResponses;

    await models.response.findAll({
      where: {
        user_id: ctx.state.user.id,
      },
    }).then((responses) => {
      myOffersResponses = responses;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = myOffersResponses;
  })
  .post('/find', async (ctx) => {
    let myOffersResponses;

    await models.response.findAll({
      where: ctx.request.body,

    }).then((responses) => {
      myOffersResponses = responses;
    }).catch(err => ctx.throw(401, err.message));
    ctx.status = 200;
    ctx.body = myOffersResponses;
  })
  .use(isUnAuth)
  .post('/', async (ctx) => {
    // if (ctx.isUnauthenticated()) {
    //   ctx.throw(401, 'Unauthenticated');
    // }
    //    create new offer_response
    const dataForCreateOfferResponse = {
      user_id: ctx.state.user.id,
      item_id: ctx.request.body.item_id,
      offer_id: ctx.params.id,
      state: stateForCreateOfferResponse,
      time: new Date().toLocaleDateString(),
    };

    await models.response.create(dataForCreateOfferResponse).then((offerResponse) => {
      ctx.status = 200;
      ctx.body = offerResponse.id;
    }).catch(err => ctx.throw(401, err.message));
  });
module.exports = router;
