'use strict';

const Router = require('koa-router');
const passport = require('../middlewares/passport'); //eslint-disable-line
const isUnAuth = require('../middlewares/isUnauthenticated');

const {
  me,
  login,
  logout,
  signup,
  root,
} = require('./RouteFunction/authFunctions');

const router = new Router();

router.post('/login', login);
router.post('/logout', logout);
router.post('/signup', signup);
router.get('/', root);
router.use(isUnAuth);
router.get('/me', me);

module.exports = router;
